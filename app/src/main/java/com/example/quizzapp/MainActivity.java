package com.example.quizzapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    private Button button_1;
    private Button button_2;
    private EditText editText_1;
    public static final String EXTRA_TEXT = "com.example.quizzapp.EXTRA_TEXT";



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button_1 = (Button) findViewById(R.id.button_1);
        button_2 = (Button) findViewById(R.id.button_2);

        button_1.setOnClickListener(v -> openActivity2());
        button_2.setOnClickListener(v -> openActivity3());
    }


    public void openActivity2(){

        editText_1 = (EditText) findViewById(R.id.editText_1);
        String text = editText_1.getText().toString();

        Intent intent = new Intent(this, Activity2.class );
        intent.putExtra(EXTRA_TEXT, text);// needs a string and a value for the String, value from userinput, string as a constant
        startActivity(intent);
    }

    public void openActivity3(){

        Intent intent = new Intent(this, Activity3.class );
        startActivity(intent);
    }
}