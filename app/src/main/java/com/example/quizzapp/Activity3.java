package com.example.quizzapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Activity3 extends AppCompatActivity {

    Button button_7;
    Button button_8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);

        button_7 = (Button) findViewById(R.id.button_7);
        button_8 = (Button) findViewById(R.id.button_8);

        button_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {openActivity4();}});

        button_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { openActivity5(); }});


    }


    public void openActivity4(){
        Intent intent = new Intent(this, Activity4.class );

        startActivity(intent);

    }

    public void openActivity5(){
        Intent intent = new Intent(this, Activity5.class );

        startActivity(intent);

    }

}