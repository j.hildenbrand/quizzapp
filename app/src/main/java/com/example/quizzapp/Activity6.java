package com.example.quizzapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class Activity6 extends AppCompatActivity {

    public TextView textView1;
    public TextView textView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_6);

        textView1= (TextView) findViewById(R.id.textView3);
        textView2= (TextView) findViewById(R.id.textView14);

        Intent intent1 = getIntent();

        int i = intent1.getIntExtra(Activity2.CORRECT_ANSWERS, 0);
        int j = intent1.getIntExtra(Activity2.INCORRECT_ANSWERS, 0);
        textView1.setText("Correct Answers: " + Integer.toString(i));
        textView2.setText("Incorrect Answers: " + Integer.toString(j));

    }
}