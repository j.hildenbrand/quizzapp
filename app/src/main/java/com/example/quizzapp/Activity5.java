package com.example.quizzapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class Activity5 extends AppCompatActivity {

    private Button saveButton;
    private Spinner spinnerNumberOfQuestions;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_5);

        saveButton = (Button) findViewById(R.id.button);
        spinnerNumberOfQuestions = (Spinner) findViewById(R.id.spinner2);
        saveButton.setOnClickListener(v -> readData());

        //initialize Spinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.numberOfQuestions, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerNumberOfQuestions.setAdapter(adapter);
    }

    public void readData(){

        int number = Integer.parseInt(spinnerNumberOfQuestions.getSelectedItem().toString());

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(getString(R.string.app_name), number);
        editor.apply();
    }





}