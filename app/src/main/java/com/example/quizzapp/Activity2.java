package com.example.quizzapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.util.prefs.Preferences;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Activity2 extends AppCompatActivity {

    private TextView textView_1, textView_2, tv2, tv4;
    private Button button_1, button_2, button_3, button_4;
    private FloatingActionButton nextButton;
    public static final String NUMBER_OF_QUESTIONS = "com.example.NUMBER_OF_QUESTIONS";
    public static final String ARRAYLIST_WITH_QUESTION_ID = "com.example.ARRAYLIST_WITH_QUESTION_ID";
    public static final String CORRECT_ANSWERS = "com.example.CORRECT_ANSWERS";
    public static final String INCORRECT_ANSWERS = "com.example.INCORRECT_ANSWERS";
    public static DatabasHelper myDB = null;
    public static int MaxNumberOfQuestions = 0;
    public static String inc_Number = null;
    public static ArrayList<Integer> arrayList = null;
    public int correct = 0;
    public int incorrect = 0;
    public String corrAnswer = null;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        myDB = new DatabasHelper(Activity2.this);
        textView_1 = (TextView) findViewById(R.id.textView_3);
        textView_2 = (TextView) findViewById(R.id.textView_2);
        tv2 = (TextView) findViewById(R.id.textView2);
        tv4 = (TextView) findViewById(R.id.textView4);
        button_1 = (Button) findViewById(R.id.button_3);
        button_2 = (Button) findViewById(R.id.button_4);
        button_3 = (Button) findViewById(R.id.button_5);
        button_4 = (Button) findViewById(R.id.button_6);
        nextButton = (FloatingActionButton) findViewById(R.id.add_button);
        button_1.setOnClickListener(v -> checkAnswer(button_1));
        button_2.setOnClickListener(v -> checkAnswer(button_2));
        button_3.setOnClickListener(v -> checkAnswer(button_3));
        button_4.setOnClickListener(v -> checkAnswer(button_4));
        //for displaying Username
        Intent intent_1 = getIntent();
        String text = intent_1.getStringExtra(MainActivity.EXTRA_TEXT);
        textView_1.setText(text);
        //for number count inc.
        Intent intent_2 = getIntent();
        inc_Number = intent_2.getStringExtra(Activity2.NUMBER_OF_QUESTIONS);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        MaxNumberOfQuestions = sharedPreferences.getInt(getString(R.string.app_name), 0);

        if (inc_Number == null) {
            String s = Integer.toString(1);
            tv4.setText(s);
            arrayList = createArrayList();
            correct = 0;
            incorrect = 0;
        } else {
            tv4.setText(inc_Number);
            Bundle extras = getIntent().getExtras();
            arrayList = extras.getIntegerArrayList(Activity2.ARRAYLIST_WITH_QUESTION_ID);
            correct = extras.getInt(Activity2.CORRECT_ANSWERS);
            incorrect = extras.getInt(Activity2.INCORRECT_ANSWERS);
        }
        nextButton.setOnClickListener(v -> nextQuestion(arrayList));
        getQuestions(2, 1, 1, 1);
    }

    void checkAnswer(Button button) {

        if (button.getText() == corrAnswer) {
            correct++;
        } else {
            incorrect++;
        }
        button_1.setClickable(false);
        button_2.setClickable(false);
        button_3.setClickable(false);
        button_4.setClickable(false);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    ArrayList<Integer> createArrayList() {

        List<Integer> range = IntStream.rangeClosed(0, MaxNumberOfQuestions - 1).boxed().collect(Collectors.toList());
        Collections.shuffle(range);
        return new ArrayList<Integer>(range);
    }


    void getQuestions(int einfach, int mittel, int schwer, int extrem) {

        int e = arrayList.get(arrayList.size() - 1);
        Cursor cursor = myDB.getQuestionsForDiff(einfach, mittel, schwer, extrem);
        cursor.moveToPosition(e);
        String string1 = cursor.getString(3); //string1 always correct answer
        corrAnswer = string1;
        String string2 = cursor.getString(4);
        String string3 = cursor.getString(5);
        String string4 = cursor.getString(6);
        String string5 = cursor.getString(1);
        String string6 = cursor.getString(2);

        Integer[] Array = {1, 2, 3, 4};
        List<Integer> intList = Arrays.asList(Array);
        Collections.shuffle(intList);
        intList.toArray(Array);

        HashMap<Integer, String> map = new HashMap<>();
        map.put(1, string1);
        map.put(2, string2);
        map.put(3, string3);
        map.put(4, string4);

        button_1.setText(map.get(Array[0]));
        button_2.setText(map.get(Array[1]));
        button_3.setText(map.get(Array[2]));
        button_4.setText(map.get(Array[3]));
        textView_2.setText(string5);
        tv2.setText(string6);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    public void nextQuestion(ArrayList<Integer> arraylist) {

        int numberOfQuestions = Integer.parseInt(tv4.getText().toString());
        int i = numberOfQuestions + 1;
        //Array for passing Question stored in cursor in random order
        arraylist.remove(arraylist.get(arraylist.size() - 1));
        StringBuilder str = new StringBuilder();
        for (int j = 0; j < arraylist.size(); j++) {
            str.append(arraylist.get(j));
        }
        if (numberOfQuestions < MaxNumberOfQuestions) {
            Intent intent_3 = new Intent(this, Activity2.class);
            intent_3.putExtra(NUMBER_OF_QUESTIONS, String.valueOf(i));
            intent_3.putExtra(ARRAYLIST_WITH_QUESTION_ID, arraylist);
            intent_3.putExtra(CORRECT_ANSWERS, correct);
            intent_3.putExtra(INCORRECT_ANSWERS, incorrect);
            startActivity(intent_3);
        } else {
            Intent intent_4 = new Intent(this, Activity6.class);
            intent_4.putExtra(CORRECT_ANSWERS, correct);
            intent_4.putExtra(INCORRECT_ANSWERS, incorrect);
            startActivity(intent_4);
        }
    }
}


