package com.example.quizzapp;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import java.util.Objects;

public class DialogNewQuestion extends AppCompatDialogFragment  {

    private EditText description, answer_1, answer_2, answer_3, answer_4;
    private ExampleDialogListener listener;
    private Spinner DifficultySpinner;

    @NonNull
    @Override
    public Dialog onCreateDialog (Bundle savedInstanceState){

        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.newquestionpopup, null);

        builder.setView(view).setTitle("Create New Question")
                .setNegativeButton("cancel", (dialoginterface, i) -> {
                    //nothing happens when canceled
                })
                .setPositiveButton("save", (dialog, which) -> {
                    String description_string = description.getText().toString();
                    String difficulty_string = DifficultySpinner.getSelectedItem().toString();
                    String answer_1_string = answer_1.getText().toString();
                    String answer_2_string = answer_2.getText().toString();
                    String answer_3_string = answer_3.getText().toString();
                    String answer_4_string = answer_4.getText().toString();

                    //when button pressed it will send data to main act.
                    listener.applyTexts(description_string, difficulty_string,answer_1_string,answer_2_string,answer_3_string,answer_4_string);
                });

        //Objects have to be find through the declared view
        description =  view.findViewById(R.id.text_1);
        DifficultySpinner = view.findViewById(R.id.spinner);
        answer_1 =  view.findViewById(R.id.text_3);
        answer_2 =  view.findViewById(R.id.text_4);
        answer_3 =  view.findViewById(R.id.text_5);
        answer_4 =  view.findViewById(R.id.text_6);

        //Spinner Adapter
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getActivity(), R.array.difficulties, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        DifficultySpinner.setAdapter(adapter);

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {

        super.onAttach(context);
        try {
            listener = (ExampleDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement ExampleDialogListener");
        }
    }


    public  interface ExampleDialogListener{

        void applyTexts(String description, String difficulty, String question1, String question2, String question3,String question4 );
    }
}

