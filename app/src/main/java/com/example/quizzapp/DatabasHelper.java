package com.example.quizzapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

public class DatabasHelper extends SQLiteOpenHelper {

    private Context context;
    private static final String DATABASE_NAME ="BookLibrary.db";
    private static final int DATABASE_VERSION = 1;

    private static final String TABEL_NAME = "my_library";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_DESCRIPTION = "description";
    private static final String COLUMN_DIFFICULTY = "difficulty";
    private static final String COLUMN_ANSWER1 = "Answer_1";
    private static final String COLUMN_ANSWER2 = "Answer_2";
    private static final String COLUMN_ANSWER3 = "Answer_3";
    private static final String COLUMN_ANSWER4 = "Answer_4";

    public DatabasHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String query = "CREATE TABLE " + TABEL_NAME + " (" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_DESCRIPTION + " TEXT, " + COLUMN_DIFFICULTY + " TEXT,  " + COLUMN_ANSWER1 + " TEXT, " + COLUMN_ANSWER2 + " TEXT, " + COLUMN_ANSWER3 + " TEXT, " + COLUMN_ANSWER4 + " TEXT);";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABEL_NAME);
        onCreate(db);
    }

    void addQuestion(String description, String difficulty, String answer1, String answer2, String answer3, String answer4){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(COLUMN_DESCRIPTION, description);
        cv.put(COLUMN_DIFFICULTY, difficulty);
        cv.put(COLUMN_ANSWER1, answer1);
        cv.put(COLUMN_ANSWER2, answer2);
        cv.put(COLUMN_ANSWER3, answer3);
        cv.put(COLUMN_ANSWER4, answer4);

        //insert Data into Table
        long result = db.insert(TABEL_NAME, null,cv);
        if(result == -1){
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(context, "Added Successfully", Toast.LENGTH_SHORT).show();
        }
    }

    //stores data from db into cursor, ref. in acitivty that wants that data
    Cursor readAllData(){

        String query = "SELECT * FROM " + TABEL_NAME; //Selects all Data db Table
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        if(db != null){
            //puts db Data into other db, stored and returned by cursor obj.
            cursor = db.rawQuery(query, null);
        }
        return cursor;
    }

    Cursor getQuestionsForDiff(int easy, int medium, int hard, int extreme){

        String query = "SELECT * FROM ( SELECT * FROM " + TABEL_NAME + " WHERE " + COLUMN_DIFFICULTY + " like 'Ein%' ORDER BY RANDOM() LIMIT " + easy + " )" +
                       " UNION " +
                       "SELECT * FROM ( SELECT * FROM " + TABEL_NAME + " WHERE " + COLUMN_DIFFICULTY + " like 'Mi%' ORDER BY RANDOM() LIMIT " + medium + " )" +
                       " UNION " +
                       "SELECT * FROM ( SELECT * FROM " + TABEL_NAME + " WHERE " + COLUMN_DIFFICULTY + " like 'Sc%' ORDER BY RANDOM() LIMIT " + hard    + " )" +
                       " UNION " +
                       "SELECT * FROM ( SELECT * FROM " + TABEL_NAME + " WHERE " + COLUMN_DIFFICULTY + " like 'Ex%' ORDER BY RANDOM() LIMIT " + extreme + " )";


        //String query = "SELECT * FROM " + TABEL_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;

        if(db != null){
            //puts db Data into other db, stored and returned by cursor obj.
            cursor = db.rawQuery(query, null);
        }
        return cursor;



    }


}
