package com.example.quizzapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class Activity4 extends AppCompatActivity implements DialogNewQuestion.ExampleDialogListener {

    FloatingActionButton addButton;
    DatabasHelper myDB;
    ArrayList<String> questionID, description, difficulty, answer1, answer2, answer3, answer4;
    RecyclerViewAdapter recyclerViewAdapter;
    RecyclerView questionList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_4);

        questionList = (RecyclerView) findViewById(R.id.questionList);
        addButton = (FloatingActionButton) findViewById(R.id.add_button);
        addButton.setOnClickListener(v -> openDialog());

        //for displaying db Data
        myDB = new DatabasHelper(Activity4.this);
        questionID = new ArrayList<>();
        description= new ArrayList<>();
        difficulty= new ArrayList<>();
        answer1= new ArrayList<>();
        answer2= new ArrayList<>();
        answer3= new ArrayList<>();
        answer4= new ArrayList<>();

        storeDataInArrays();

        recyclerViewAdapter = new RecyclerViewAdapter(Activity4.this, questionID, description, difficulty, answer1,answer2,answer3,answer4 );
        questionList.setAdapter(recyclerViewAdapter);
        questionList.setLayoutManager(new LinearLayoutManager(Activity4.this));
    }

    void storeDataInArrays(){
        Cursor cursor = myDB.readAllData();
        if(cursor.getCount() == 0){
            Toast.makeText(this, "NO DATA.", Toast.LENGTH_SHORT).show();
        }else{
            while(cursor.moveToNext()){
                questionID.add(cursor.getString(0));
                description.add(cursor.getString(1));
                difficulty.add(cursor.getString(2));
                answer1.add(cursor.getString(3));
                answer2.add(cursor.getString(4));
                answer3.add(cursor.getString(5));
                answer4.add(cursor.getString(6));

            }
        }
    }

     public void openDialog(){

        DialogNewQuestion exampleDialog = new DialogNewQuestion();
        exampleDialog.show(getSupportFragmentManager(), "example dialog");
    }

    //function lets popupWindow communicate wth parent,
    @Override
    public void applyTexts(String description, String difficulty, String question1, String question2, String question3, String question4) {
        DatabasHelper myDB = new DatabasHelper(Activity4.this);
        //adds data into db that has been passed from the dialog
        myDB.addQuestion(description, difficulty, question1, question2, question3,question4);

    }


}






