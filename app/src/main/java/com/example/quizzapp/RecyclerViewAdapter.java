package com.example.quizzapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private Context context;
    private ArrayList questionID, description, difficulty, answer1, answer2, answer3, answer4;

    public RecyclerViewAdapter(Context context, ArrayList questionID,  ArrayList description, ArrayList difficulty,
                        ArrayList answer1, ArrayList answer2, ArrayList answer3, ArrayList answer4){

        this.context = context;
        this.questionID = questionID;
        this.description = description;
        this.difficulty = difficulty;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.answer3 = answer3;
        this.answer4 = answer4;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView questionID, description, difficulty, answer1, answer2, answer3, answer4;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            questionID = itemView.findViewById(R.id.textView5);
            description= itemView.findViewById(R.id.textView6);
            difficulty= itemView.findViewById(R.id.textView12);
            answer1= itemView.findViewById(R.id.textView8);
            answer2= itemView.findViewById(R.id.textView9);
            answer3= itemView.findViewById(R.id.textView10);
            answer4= itemView.findViewById(R.id.textView11);
        }
    }

    @NonNull
    @Override
    public RecyclerViewAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.question_row, parent, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter.MyViewHolder holder, int position) {
        //set Text
        holder.questionID.setText(String.valueOf(questionID.get(position)));
        holder.description.setText(String.valueOf(description.get(position)));
        holder.difficulty.setText(String.valueOf(difficulty.get(position)));
        holder.answer1.setText(String.valueOf(answer1.get(position)));
        holder.answer2.setText(String.valueOf(answer2.get(position)));
        holder.answer3.setText(String.valueOf(answer3.get(position)));
        holder.answer4.setText(String.valueOf(answer4.get(position)));
    }

    @Override
    public int getItemCount() {
        return questionID.size();
    }
}
